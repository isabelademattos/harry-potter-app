## Harry Potter APP

### Clone o repositório
- git clone git@gitlab.com:isabelademattos/harry-potter-app.git
- cd harry-potter-app
- yarn
- yarn start
- Rodar aplicação em http://localhost:3001/

### Check-list
- Criar novo personagem, escolha da casa através do chapéu seletor
- Editar personagem
- Excluir personagem
- Listar todos os personagens
- Filtro de personagem por casa

### To-do-list
- Implementar i18n para não usar apenas o português
- Implementar testes para componentes e requisições à API
- Implementar tratativas nas requisições ao falhar
- Implementar docker
