import React, { Suspense, lazy } from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Container from './components/container/Container';

/* Used to render a lazy component with react-router */
const waitFor = (Tag) => (props) => <Tag {...props} />;

const CharactersView = lazy(() => import("./components/view/CharactersView"));
const CharactersForm = lazy(() => import("./components/form/CharactersForm"));

const Routes = ({ location }) => {
  const currentKey = location.pathname.split("/")[1] || "/";
  const timeout = { enter: 500, exit: 500 };
  const animationName = "rag-fadeIn";

  const PageLoader = () => (
    <div className="page-loader">
      <em className="fas fa-circle-notch fa-spin fa-2x text-muted"></em>
    </div>
  )

  return (
    <Container>
      <TransitionGroup>
        <CSSTransition
          key={currentKey}
          timeout={timeout}
          classNames={animationName}
          exit={false}
        >
          <div>
            <Suspense fallback={<PageLoader />}>
              <Switch location={location}>
                <Route
                  exact
                  path="/"
                  component={waitFor(CharactersView)}
                />
                <Route
                  exact
                  path="/characters"
                  component={waitFor(CharactersView)}
                />
                <Route
                  exact
                  path="/characters/new"
                  component={waitFor(CharactersForm)}
                />
                <Route
                  exact
                  path="/characters/:id/edit"
                  component={waitFor(CharactersForm)}
                />
              </Switch>
            </Suspense>
          </div>
        </CSSTransition>
      </TransitionGroup>
    </Container>
  );
};

export default withRouter(Routes);
