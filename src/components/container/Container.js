import React from 'react';
import Card from '@material-ui/core/Card';
import "./style.scss";

const Container = (props) => {
  return (
    <div className="container-card">
      <Card className="container-card-content">
        {props.children}
      </Card>
    </div>
  );
}

export default Container;
