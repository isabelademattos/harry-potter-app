import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { headers, myPotterApi } from '../../Services'

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },

  textField: {
    width: "100%",
  },

  button: {
    width: "100%",
    height: "100%"
  },
}));

const initialState = {
  name: "",
  role: "",
  school: "",
  house: "",
  houseName: "",
  patronus: ""
}

const CharactersForm = (props) => {
  const characterId = props.match.params.id;
  const [character, setCharacter] = useState(initialState);
  const [houses, setHouses] = useState([]);
  const [charactersView, setCharactersView] = useState(false);
  const classes = useStyles();

  useEffect(() => {
    getHouses();
    // if (characterId) {
    //   fetchData();
    // }
  }, []);

  useEffect(() => {
    // getHouses();
    if (characterId) {
      fetchData();
    }
  }, [houses]);

  async function getHouses() {
    await myPotterApi.get("houses", headers)
    .then(function(response) {
      setHouses(response.data);
    })
    .catch(function(error) {
      toast("Erro ao carregar casas", {
        type: "error",
        position: "top-right",
      });
    });
  }

  async function fetchData() {
    await myPotterApi.get(`characters/${characterId}`, headers)
    .then(function(response) {
      setCharacter({
        ...character,
        name: response.data.name,
        role: response.data.role,
        school: response.data.school,
        house: response.data.house,
        houseName: findHouse(response.data.house),
        patronus: response.data.patronus
      })
    })
    .catch(function(error) {
      console.error(error);
      toast("Erro ao carregar informações", {
        type: "error",
        position: "top-right",
      });
    });
  }

  const findHouse = (houseId) => {
    const house = houses.find(result => result._id === houseId) || "erro";
    return house.name;
  }

  const handleChange = (event) => {
    setCharacter({
      ...character,
      [event.target.name]: event.target.value
    });
  };

  const handomHouse = () => {
    if (houses.length > 0) {
      let selectedHouse = houses[Math.floor(Math.random()*houses.length)];
      setCharacter({
        ...character,
        house: selectedHouse._id,
        houseName: selectedHouse.name
      });
    }
  }

  const clearFields = () => {
    setCharacter(initialState);
  }

  async function handleSubmit() {
    if (characterId) {
      await myPotterApi.put(`characters/${characterId}`, character, headers)
      .then(function(response) {
        toast("Salvo com sucesso", {
          type: "success",
          position: "top-right",
        });
      })
      .catch(function(error) {
        toast("Erro ao salvar", {
          type: "error",
          position: "top-right",
        });
      });
    } else {
      await myPotterApi.post("characters", character, headers)
      .then(function(response) {
        toast("Salvo com sucesso", {
          type: "success",
          position: "top-right",
        });
      })
      .catch(function(error) {
        toast(error.response.data.message, {
          type: "error",
          position: "top-right",
        });
      });
    }
  }

  const handleBack = () => {
    setCharactersView(true)
  }

  if (charactersView) {
    return <Redirect to={"/characters"} />;
  }

  return (
    <div className={classes.root}>
      <h1>NOVO PERSONAGEM</h1>
      <form noValidate autoComplete="off">
        <Grid container spacing={2}>
          <ToastContainer />
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              id="name"
              name="name"
              label="Nome"
              value={character.name}
              onChange={(e) => handleChange(e)}
              className={classes.textField}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              id="role"
              name="role"
              label="Posição"
              value={character.role}
              onChange={(e) => handleChange(e)}
              className={classes.textField}
              variant="outlined"
            />
          </Grid>
          {/*  */}
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              id="school"
              name="school"
              label="Escola"
              value={character.school}
              onChange={(e) => handleChange(e)}
              className={classes.textField}
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              id="patronus"
              name="patronus"
              label="Patrono"
              value={character.patronus}
              onChange={(e) => handleChange(e)}
              className={classes.textField}
              variant="outlined"
            />
          </Grid>
          {/*  */}
          <Grid item xs={12} sm={6}>
            <Button
              variant="outlined"
              color="primary"
              size="large"
              className={classes.button}
              onClick={() => handomHouse()}
            >
              CHAPÉU SELETOR
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              fullWidth
              id="house"
              name="house"
              label="Casa"
              value={character.houseName}
              onChange={(e) => handleChange(e)}
              className={classes.textField}
              variant="outlined"
              disabled={true}
            />
          </Grid>
          {/*  */}
          <Grid item xs={12} sm={3}>
            <Button
              variant="contained"
              color="default"
              size="large"
              className={classes.button}
              onClick={() => handleBack()}
            >
              VOLTAR
            </Button>
          </Grid>
          <Grid item xs={12} sm={3}></Grid>
          <Grid item xs={12} sm={3}>
            <Button
              variant="contained"
              color="default"
              size="large"
              className={classes.button}
              onClick={() => clearFields()}
            >
              LIMPAR
            </Button>
          </Grid>
          <Grid item xs={12} sm={3}>
            <Button
              variant="contained"
              color="primary"
              size="large"
              className={classes.button}
              onClick={handleSubmit}
            >
              SALVAR
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

export default CharactersForm;
