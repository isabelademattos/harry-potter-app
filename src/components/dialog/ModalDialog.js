import React, { forwardRef } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Slide,
} from '@material-ui/core';
import Draggable from 'react-draggable';

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const ModalDialog = (props) => {
  const { open, setOpen, msg, setConfirm } = props;

  const handleClose = (param) => () => {
    setConfirm(param);
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        disableBackdropClick={true}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          {msg.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>{msg.content}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose(false)} color="primary">
            {msg.btnCancel}
          </Button>
          <Button onClick={handleClose(true)} color="primary">
            {msg.btnConfirm}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ModalDialog;
