import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  IconButton,
  Tooltip,
  Button,
  Grid,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Input
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import { Card, CardHeader, CardFooter } from 'reactstrap';
import ModalDialog from '../dialog/ModalDialog'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { headers, myPotterApi } from '../../Services'

function createData(id, name, role, school) {
  return { id, name, role, school };
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    name: "name",
    numeric: false,
    disablePadding: false,
    label: "Nome",
    sort: true,
  },
  {
    name: "role",
    numeric: false,
    disablePadding: false,
    label: "Posição",
    sort: true,
  },
  {
    name: "school",
    numeric: false,
    disablePadding: false,
    label: "Escola",
    sort: true,
  },
  {
    name: "actions",
    numeric: false,
    disablePadding: false,
    label: "",
    sort: false,
  },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.name}
            align={headCell.numeric ? "right" : "left"}
            padding={"default"}
            sortDirection={orderBy === headCell.name ? order : false}
          >
            {headCell.sort ? (
              <TableSortLabel
                active={orderBy === headCell.name}
                direction={orderBy === headCell.name ? order : "asc"}
                onClick={createSortHandler(headCell.name)}
              >
                {headCell.label}
                {orderBy === headCell.name ? (
                  <span className={classes.visuallyHidden}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </span>
                ) : null}
              </TableSortLabel>
            ) : (
              headCell.label
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },

  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },

  table: {
    minWidth: 750,
  },

  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },

  row: {
    margin: "1px",
    paddingTop: "10px"
  },

  paperButton: {
    textTransform: "none",
  },

  button: {
    width: "100%",
    height: "100%"
  }
}));

const CharactersView = (props) => {
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("calories");
  const [selected, setSelected] = useState([]);
  const [rows, setRows] = useState([]);
  const [characterNew, setCharacterNew] = useState(false);
  const [characterEditId, setCharacterEditId] = useState("");
  const [characterId, setCharacterId] = useState("");
  const [openModalDialog, setOpenModalDialog] = useState(false);
  const [confirm, setConfirm] = useState(false);
  const [houses, setHouses] = useState([]);
  const [msgModal, setMsgModal] = useState({title: "", content: "", btnCancel: "", btnConfirm: ""});
  const [selectedHouse, setSelectedHouse] = useState("");

  useEffect(() => {
    fetchData();
    getHouses();
  }, []);

  async function fetchData() {
    await myPotterApi.get("characters", headers)
    .then(function(response) {
      const characters = response.data.map((character) => {
        return createData(
          character.id,
          character.name,
          character.role,
          character.school
        );
      });
      setRows(characters);
    })
    .catch(function(error) {
      toast("Erro ao carregar personagens", {
        type: "error",
        position: "top-right",
      });
    });
  }

  async function getHouses() {
    await myPotterApi.get("houses", headers)
    .then(function(response) {
      setHouses(response.data);
    })
    .catch(function(error) {
      toast("Erro ao carregar casas", {
        type: "error",
        position: "top-right",
      });
    });
  }

  async function filterHouse(houseId) {
    await myPotterApi.get(`characters/houses/${houseId}`, headers)
    .then(function(response) {
      const characters = response.data.map((character) => {
        return createData(
          character.id,
          character.name,
          character.role,
          character.school
        );
      });
      setRows(characters);
    })
    .catch(function(error) {
      toast("Erro ao carregar personagens", {
        type: "error",
        position: "top-right",
      });
    });
  }

  const handleRequestSort = (_, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleEdit = (id) => () => {
    setCharacterEditId(id);
    setCharacterNew(false);
  };

  const handleDelete = (obj) => () => {
    setMsgModal({
      title: "Deletar personagem",
      content: "Deseja realmente deletar o personagem?",
      btnCancel: "Cancelar",
      btnConfirm: "Sim",
    });
    setOpenModalDialog(true);
    setCharacterId(obj.id);
  };

  const handleNewCharacter = () => {
    setCharacterEditId("");
    setCharacterNew(true);
  };

  const handleChange = (event) => {
    setSelectedHouse(event.target.value);
    filterHouse(event.target.value);
  };

  if (confirm) {
    setConfirm(false);

    (async () => {
      await myPotterApi.delete(`characters/${characterId}`, headers)
      .then(function(response) {
        toast("Excluído com sucesso", {
          type: "success",
          position: "top-right",
        });
        fetchData();
      })
      .catch(function(error) {
        toast("Erro ao excluir personagem", {
          type: "error",
          position: "top-right",
        });
      });
    })();
  }

  if (characterNew) {
    return <Redirect to={"characters/new"} />;
  }

  if (characterEditId.length > 0) {
    return <Redirect to={`/characters/${characterEditId}/edit`} />;
  }

  return (
    <div className={classes.root}>
      <h1>LISTA DE PERSONAGENS</h1>
      <div>
        <Card className="card-default">
          <CardHeader>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={9}>
              <FormControl fullWidth>
                <InputLabel>
                  SELECIONAR CASA
                </InputLabel>
                <Select
                  fullWidth
                  autoFocus={true}
                  id="selectHouses"
                  variant="outlined"
                  name="selectHouses"
                  value={selectedHouse}
                  onChange={(e) => handleChange(e)}
                  input={<Input />}
                >
                  {houses.map((item, index) => {
                    return (
                      <MenuItem key={index} value={item._id}>
                        {item.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                className={classes.button}
                onClick={() => fetchData()}
              >
                LIMPAR FILTRO
              </Button>
            </Grid>
          </Grid>

          </CardHeader>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .map((row) => {
                    return (
                      <TableRow hover tabIndex={-1} key={row.id}>
                        <TableCell align="left">{row.name}</TableCell>
                        <TableCell align="left">{row.role}</TableCell>
                        <TableCell align="left">{row.school}</TableCell>
                        <TableCell align="left">
                          <Tooltip title="Editar">
                            <IconButton
                              aria-label="edit"
                              onClick={handleEdit(row.id)}
                            >
                              <EditIcon />
                            </IconButton>
                          </Tooltip>
                          <Tooltip title="Deletar">
                            <IconButton
                              aria-label="edit"
                              onClick={handleDelete({
                                id: row.id,
                                name: row.name,
                              })}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
        </Card>
        <CardFooter className={classes.row}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={9}></Grid>
            <Grid item xs={12} sm={3}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                className={classes.button}
                onClick={handleNewCharacter}
              >
                NOVO
              </Button>
            </Grid>
          </Grid>
        </CardFooter>
        <ModalDialog
          open={openModalDialog}
          setOpen={setOpenModalDialog}
          msg={msgModal}
          setConfirm={setConfirm}
        />
        <ToastContainer />
      </div>
    </div>
  );
};

export default CharactersView;
