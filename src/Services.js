import axios from 'axios';

export const headers = {
  headers: { 'Content-Type': 'application/json' }
};

export  const myPotterApi = axios.create({
  baseURL: 'http://localhost:3000/api/v1',
});
